/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oxprogram2;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Game { 
    private Scanner kb = new Scanner(System.in);
    private Table table = null;
    private int row,col;
    private Player o = null;
    private Player x = null;
    private int countCycle=0;
    private char inputcontinue ;
    
    public void run(){
        this.showWelcome();
        this.table = new Table(o,x);
        while(inputcontinue != 'n'){
            this.showTable();
            this.showTurn();
            this.inputRowcol();
            if(table.checkWin() ){
                this.showTable();
                System.out.println("Player "+table.getCurrentPlayer().getName()+" Win...");
                this.updateScore();
                this.showScore();
                this.askContinue();
                if(inputcontinue == 'y'){
                   goContinue();
                }else if(inputcontinue == 'n'){
                   return;
                }
            }
                table.switchPlayer();
                    countCycle++;
            
                if(countCycle==9){
                    draw();
                }
        }
        this.showBye();
    }
    
    public Game(){
        this.o = new Player('o');
        this.x = new Player('x');
    }
    
    public void startGame(){
        this.showWelcome();
    }
    
    public void showWelcome(){
        System.out.println("Welcome to OX Game");
    }
    
    public void showTable(){
        char[][] data = this.table.getData();
        for(int row=0 ; row<data.length; row++){
            for(int col=0 ; col<data[row].length;col++){
                System.out.print(data[row][col]+" ");
            }
                System.out.println("");
        }
    }
    
    public void showTurn(){
         System.out.println("Turn "+table.getCurrentPlayer().getName());
    }
    
    
    public void inputRowcol(){
        while(true){
            this.input();
        try{
        if(table.setRowCol(row,col))
            return;
        }catch(ArrayIndexOutOfBoundsException e){
            System.out.println("Please input number 1-3!!");
        }
      }
    }
    
    public void input(){
        while(true){
        try{
            System.out.print("Please input row col: ");
            this.row = kb.nextInt();
            this.col = kb.nextInt();
            return;
        }catch(InputMismatchException E){
            kb.next();
            System.out.print("Please input number 1-3!!");
        }
        System.out.println();
    }
    }
    
    public void updateDraw(){
        o.addDraw();
        x.addDraw();
    }
    
    public void updateScore(){
        if(table.getCurrentPlayer().getName() == 'o'){
            o.addWin();
            x.addLose();
        }else if(table.getCurrentPlayer().getName() == 'x'){
            x.addWin();
            o.addLose();
        }
    }
    
    public void askContinue(){
        System.out.print("Continue? (y/n): ");
        inputcontinue = kb.next().charAt(0);
    }
    
    public void showDraw(){
        System.out.println("Draw in this game...");
    }
    
    public void showScore(){
        System.out.println("X Win:" + x.getWin() + " Lose:" + x.getLose()
                + " Draw:" + x.getDraw());
        System.out.println("O Win:" + o.getWin() + " Lose:" + o.getLose()
                + " Draw:" + o.getDraw());
    }

    public void goContinue(){
        table.clearTable();
        countCycle =- 1;
        System.out.println("====================");
        this.showWelcome();
    }

    public void draw(){
        this.showTable();
        this.showDraw();
        this.updateDraw();
        this.showScore();
        this.askContinue();
        if(inputcontinue == 'y'){
            goContinue();
        }else if(inputcontinue == 'n'){
            return;
        }
    }
    
    public void showBye(){
         System.out.println("Bye bye ....");
    }
    
}
