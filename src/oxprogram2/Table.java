/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oxprogram2;

import java.util.Scanner;

public class Table {
    private char[][] data = { {'-','-','-'},
                               {'-','-','-'},
                               {'-','-','-'} };
    
    private Player first;
    private Player second;
    private Player currentPlayer;
    private Player winner;
    

    public Table(Player first, Player second) {
        this.first = first;
        this.second = second;
        this.currentPlayer = this.first;
    }
 
    
    public boolean setRowCol(int row,int col){
        if(this.data[row-1][col-1]!='-'){
            return false;
        }
        this.data[row -1][col -1] = currentPlayer.getName();
        return true;
    }
    
    public boolean checkWin(Player player){
        return false;
    }

    
    public void switchPlayer(){
         if(currentPlayer == first){
            this.currentPlayer = second;
        }else if(currentPlayer == second){
            currentPlayer = first;
        }
    }
    
    public void clearTable(){
         for(int row=0 ; row<data.length; row++){
            for(int col=0 ; col<data[row].length;col++){
                this.data[row][col] = '-';
            }
        }
    }
    
    
    public Player getCurrentPlayer(){
        return currentPlayer;
    }
    
    public char[][] getData(){
        return data;
    }
    
    public boolean checkWin(){
        checkWinrow();
        if(checkWinrow() || checkWincol() || checkWincrosswise()){
            return true;
        }
        return false;
    }
    
    public boolean checkWinrow() {
        if(data[0][0] == currentPlayer.getName() && data[0][1] == currentPlayer.getName()
                && data[0][2] == currentPlayer.getName()){
            return true;
        }else if (data[1][0] == currentPlayer.getName() && data[1][1] == currentPlayer.getName()
                && data[1][2] == currentPlayer.getName()){
            return true;
        }else if(data[2][0] == currentPlayer.getName() && data[2][1] == currentPlayer.getName()
                && data[2][2] == currentPlayer.getName()){
            return true;
        }
        return false;
    }
    
    public boolean checkWincol() {
        if(data[0][0] == currentPlayer.getName() && data[1][0] == currentPlayer.getName()
                && data[2][0] == currentPlayer.getName()){
            return true;
        }else if (data[0][1] == currentPlayer.getName() && data[1][1] == currentPlayer.getName()
                && data[2][1] == currentPlayer.getName()){
            return true;
        }else if(data[0][2] == currentPlayer.getName() && data[1][2] == currentPlayer.getName()
                && data[2][2] == currentPlayer.getName()){
            return true;
        }
        return false;
    }
    
    public boolean checkWincrosswise(){
       if(data[0][0] == currentPlayer.getName() && data[1][1] == currentPlayer.getName()
                && data[2][2] == currentPlayer.getName()){
            return true;
        }else if (data[0][2] == currentPlayer.getName() && data[1][1] == currentPlayer.getName()
                && data[2][0] == currentPlayer.getName()){
            return true;
        }
        return false;
    }
    
    
    
   
    
}
