/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.Test ;
import static org.junit.Assert.* ;
import oxprogram2.Table;
import oxprogram2.Player;

/**
 *
 * @author LIGHTsOUT
 */
public class TableUnitTest {
    
    public TableUnitTest() {
    }
    
    
@Test
     public void testRow1Win(){
        Player o = new Player('0');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(1,1);
        table.setRowCol(1,2);
        table.setRowCol(1,3);
        assertEquals(true,table.checkWin());

    }
     public void testCol1Win(){
       Player o = new Player('o');
       Player x = new Player('x');
       Table table = new Table(o,x);
       table.setRowCol(1,1);
       table.setRowCol(2,1);
       table.setRowCol(3,2);
       assertEquals(true,table.checkWin());
           
       
   }
      public void testCol3Win(){
       Player o = new Player('o');
       Player x = new Player('x');
       Table table = new Table(o,x);
       table.setRowCol(1,3);
       table.setRowCol(2,3);
       table.setRowCol(3,3);
       assertEquals(true,table.checkWin());
           
       
   }
      public void testX1Win(){
       Player o = new Player('o');
       Player x = new Player('x');
       Table table = new Table(o,x);
       table.setRowCol(1,1);
       table.setRowCol(2,2);
       table.setRowCol(3,3);
       assertEquals(true,table.checkWin());
           
       
   }
      public void testX2Win(){
       Player o = new Player('o');
       Player x = new Player('x');
       Table table = new Table(o,x);
       table.setRowCol(1,3);
       table.setRowCol(2,2);
       table.setRowCol(3,1);
       assertEquals(true,table.checkWin());
           
       
   }
       public void testSwitchPlayer(){
        Player o = new Player('o');
       Player x = new Player('x');
       Table table = new Table(o,x);
       table.switchPlayer();
       assertEquals('x',table.getCurrentPlayer().getName());
    }
       public void  testRow2Win(){
        Player o = new Player('0');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(2,1);
        table.setRowCol(2,2);
        table.setRowCol(2,3);
        assertEquals(true,table.checkWin());
     }
       
      public void  testRow3Win(){
        Player o = new Player('0');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(3,1);
        table.setRowCol(3,2);
        table.setRowCol(3,3);
        assertEquals(true,table.checkWin());
     }
      public void testSetRowCol(){
        Player o = new Player('0');
        Player x = new Player('x');
        Table table = new Table(o,x);
        for(int i=1;i<=3;i++){
            for(int j=1;j<=3;j++){
                 table.setRowCol(i,j);
                 assertEquals(false,table.setRowCol(i,j));
            }
        }
    }
      public void  testCol2Win(){
        Player o = new Player('0');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(1,2);
        table.setRowCol(2,2);
        table.setRowCol(3,2);
        assertEquals(true,table.checkWin());
     }




}
